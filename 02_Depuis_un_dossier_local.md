# Créer un dépôt GitLab pour votre répertoire local

#### **Situation** : votre super zouli projet est sur votre ordinateur et vous devez le mettre sur le raspberry dans deux minutes, comment faire ?

* Vous avez un dossier local nommé : "i_love_git_local" et vous souhaitez le mettre sur GitLab pour pouvoir le cloner sur le raspberry rapidement avant que Fred ne se mette à crier : "*Alors c'est livré ?*"

#### Deux solutions s'offrent à vous : la **simple** et la **complexe**

## **Méthode simple**
* Créez un nouveau projet sur GitLab, nommez le comme vous le souhaitez (par exemple : "i_love_git_gitlab")
![alt text](images/new_project.jpg)
* Faites un **```git clone```** de ce dépôt en local
* Copiez le contenu du répertoire "i_love_git_local" dans le répertoire "i_love_git_gitlab" créé par *Git* (si les deux ont le même nom, ça peut poser des problèmes lors du clonage)
* Faites un **```git add .```**, puis **```git commit```** et un **```git push```** pour mettre à jour le dépôt GitLab depuis votre dépôt local


## **Méthode complexe**
* Créez un nouveau projet sur GitLab, nommez le comme vous le souhaitez (par exemple : "i_love_git_gitlab")
* Dans votre répertoire local "i_love_git_local", faites un **```git init```** depuis le terminal pour initialiser un lien avec *Git*
* Copiez l'url de votre dépôt GitLab *i_love_git_gitlab* comme vu dans le tutoriel 01
* Depuis le terminal, tapez **```git remote add origin <url>```** pour lier votre dépôt git local et votre dépôt GitLab. ***"origin"*** est le nom distant pour votre dépôt, vous pouvez mettre ce que vous souhaitez mais par convention, on l'appelle l'**origin**
* Pour mettre à jour votre dépôt GitLab, faites un **```git add .```**, puis **```git commit```** et un **```git push origin master```**. Ici, il sera nécessaire d'utiliser **origin master** pour le **git push** puisque la branche aura été créée manuellement avec **```git remote add origin <url>```** 


### Comment se débarasser du ***origin master*** lorsque qu'on a utilisé la méthode complexe ?
*Cette méthode vous est offerte par Nora*  
Il faut dire à la *branch* ***master*** de *tracker* (de suivre les changements) de l'*origin*. En utilisant la méthode simple, cela se fait automatiquement mais ici, nous devons le faire manuellement.
Il suffit de taper **```git branch -u origin/master```** et *Git* vous dira que ***master*** est paramétrée. Vous pouvez le vérifier en tapant **```git branch -vv```**

![alt text](images/origin_master.png)

# Et le raspberry ?
* Connectez vous avec le terminal : **```ssh <prenom>@face.rct```** (le mot de passe étant votre prénom)
* Déplacez dans l'arborescence de fichiers avec le terminal (indice : utilisez ```cd```) et une fois que vous êtes là où vous souhaitez cloner votre dépôt, faites un **```git clone <url>```** (l'url étant celle de votre dépôt GitLab créé un peu plus tôt)