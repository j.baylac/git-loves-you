# Mettre à jour un dépôt git

#### **Situation** : votre dépôt sur le raspberry vient d'être *push* mais vous venez de vous rendre compte que vous aviez oublié d'ajouter votre superbe fonction Python pour compter les moutons, comment faire ?

Ici, nous supposerons que vous avez un **dépôt GitLab avec votre projet qui** __***n'est pas à jour***__ et un **dépôt local sur votre ordinateur** __***que vous mettez à jour***__

* Ajoutez votre fonction, depuis votre dépôt local sur votre ordinateur, avec n'importe quel IDE
* Faites un **```git add .```**, puis **```git commit```** et un **```git push```** (ou **git push origin master** si vous avez créé votre branche avec la méthode complexe du tuto 02) pour mettre à jour votre dépôt GitLab
* Retournez sur le raspberry où vous avez déjà cloné votre dépôt GitLab. Allez dans le répertoire de votre projet et faites **```git pull```** pour contacter GitLab afin d'en récupérer les mises à jour. Si la branche a été créée manuellement, *Git* vous demandera de faire un **git pull origin master**